import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.log4j.Logger;

import java.io.IOException;


public class YoutubeMapper {

    public static final Logger LOGGER = Logger.getLogger(YoutubeMapper.class);;


    public static class Map extends Mapper<LongWritable, Text, Text,
            Text> {

        private Text video_category = new Text();
        private Text video_name = new Text();
        public void map(LongWritable key, Text value, Context context )
                throws IOException, InterruptedException {
            String line = value.toString();

            if(line.length() > 0) {
                String[] str=line.split("\\t");
                video_name.set(str[0] + "\t" + str[5]);
                video_category.set(str[3]);
            }
            context.write(video_category, video_name);
        }
    }
}
