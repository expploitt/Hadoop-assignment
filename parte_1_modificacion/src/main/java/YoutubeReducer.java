import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

public class YoutubeReducer {

    public static class Reduce extends Reducer<Text, Text,Text, Text> {

        private static TreeMap<Integer, String> bestValues = new TreeMap<>();

        @Override
        protected void reduce(Text key, Iterable<Text> values, Context context)
                throws IOException, InterruptedException {

            String video_id  = "";
            int views = 0;
            for (Text val : values) {
                String[] str = val.toString().split("\t");

                video_id = str[0];
                views = Integer.parseInt(str[1]);

                bestValues.put(views, video_id);
                if(bestValues.size() > 1){
                    bestValues.remove(bestValues.firstKey());
                }
            }

            context.write(key, new Text(video_id + "\t" + bestValues.firstKey()));
            bestValues.clear();
        }
    }
}

