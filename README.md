# Hadoop Asignment

## Parte 1

Los comando necesarios para construir el proyecto
```
$ cd parte_1
$ gradlew build
$ cp build/libs/youtube_map_reduce-1.0-SNAPSHOT.jar <ruta_a_raiz_hadoop>
```

Configurar el dataset en el HDFS de hadoop
```
# Generamos el sistema de ficheros de Hadoop
$ bin/hdfs namenode -format

# Inicializamos Hadoop
$ sbin/start-dfs.sh

# Configuramos la ruta de archivos en el sistema Hdfs
$ bin/hdfs dfs -mkdir /user
$ bin/hdfs dfs -mkdir /user/<username>

# Copiamos el archivo de local a el sistema Hdfs
$ bin/hdfs dfs -copyFromLocal youtubedata /user/<user>/

# Ejecutamos el jar
$ bin/hadoop jar youtube_map_reduce-1.0-SNAPSHOT.jar YoutubeAnalysis /user/<user>/youtubedata resultado

# Ordenamos y obtenemos el top 10 como salida
$ bin/hadoop fs -cat /user/expploitt/resultado/part-r-00000 | sort -n -k2 -r | head -n10
```



## Parte 1 - Modificación

El proyecto *parte1_modification* encuentra el vídeo más *views* para cada categoría. Por tanto, dará como salida tantas líneas como categorías existan.

Los comando necesarios para construir el proyecto
```
$ cd parte_1_modification
$ gradlew build
$ cp build/libs/youtube_map_reduce_modification-1.0-SNAPSHOT.jar <ruta_a_raiz_hadoop>


$ bin/hadoop jar youtube_map_reduce_modification-1.0-SNAPSHOT.jar YoutubeAnalysis youtubedata resultado
$ bin/hdfs dfs -get resultado_modication resultado_mod

```

## Parte 2 

Primeramente es necesario ejecutar los siguientes comando en R para generar el dataset que se analizará por el cluster hadoop.

```
> ratings <- read.csv("ratings.csv")
> movies <- read.csv("movies.csv")
> movies_merge <- merge(ratings, movies, by="movieId")
> write.csv(movies_merge, "movie_dataset.csv", sep="\t")
```

Construimos el jar
```
$ cd parte_2
$ gradle build
$ cp build/libs/movies_map_reduce-1.0-SNAPSHOT.jar <ruta_a_raiz_hadoop>

# Copiamos el archivo de local a el sistema Hdfs
$ bin/hdfs dfs -copyFromLocal movie_dataset.csv /user/<user>/

# Ejecutamos el jar
$ bin/hadoop jar movies_map_reduce-1.0-SNAPSHOT.jar MoviesAnalysis /user/<user>/movie_dataset.csv resultado

```
