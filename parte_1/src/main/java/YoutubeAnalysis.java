import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class YoutubeAnalysis {
    public static void main(String[] args) throws Exception{
        Configuration conf = new Configuration();
        Job job;
        job = Job.getInstance(conf, "youtube analysis job");
        job.setJarByClass(YoutubeAnalysis.class);
        job.setMapperClass(YoutubeMapper.Map.class);
        //job.setCombinerClass(YoutubeReducer.Reduce.class);
        job.setReducerClass(YoutubeReducer.Reduce.class);
        job.setReducerClass(YoutubeReducer.Reduce.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(FloatWritable.class);
        FileInputFormat.addInputPath(job, new Path(args[0])); /*"/user/expploitt/youtubedata"*/
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
