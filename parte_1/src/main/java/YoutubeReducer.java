import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

public class YoutubeReducer {
    public static class Reduce extends Reducer<Text, FloatWritable,Text, FloatWritable> {

        public static TreeMap<String, Float> bestValues = new TreeMap<>();

        public void reduce(Text key, Iterable<FloatWritable> values, Context context)
                throws IOException, InterruptedException {

            float sum = 0;
            int l=0;
            for (FloatWritable val : values) {
                l+=1;
                sum += val.get();
            }
            sum=sum/l;
            context.write(key, new FloatWritable(sum));
        }
    }
}

