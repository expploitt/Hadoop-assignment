import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.log4j.Logger;

import java.io.IOException;


public class YoutubeMapper {

    public static final Logger LOGGER = Logger.getLogger(YoutubeMapper.class);;


    public static class Map extends Mapper<LongWritable, Text, Text,
            FloatWritable> {

        private Text video_name = new Text();
        private FloatWritable rating = new FloatWritable();
        public void map(LongWritable key, Text value, Context context )
                throws IOException, InterruptedException {
            String line = value.toString();

            if(line.length() > 0 && !line.equals("\n")) {
                String[] str=line.split("\\t");
                    video_name.set(str[0]);
                    for (String s : str
                    ) {
                        LOGGER.info(s + ",");
                    }
                    LOGGER.info("\n");
                    if (str[6].matches("\\d+.+")) {
                        float f = Float.parseFloat(str[6]);
                        rating.set(f);
                    }
            }
            context.write(video_name, rating);
        }
    }
}
