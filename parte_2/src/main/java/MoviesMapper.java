import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;


public class MoviesMapper {

    public static final Logger LOGGER = Logger.getLogger(MoviesMapper.class);;


    public static class Map extends Mapper<LongWritable, Text, Text,
            FloatWritable> {

        private List<Text> genres = new LinkedList<>();
        private FloatWritable rate = new FloatWritable();
        public void map(LongWritable key, Text value, Context context )
                throws IOException, InterruptedException {
            String line = value.toString().replaceAll("\"", "");

            try {
                if (line.length() > 0 && !line.contains("#")) {
                    String[] str = line.split(",");
                    rate.set(Float.parseFloat(str[3]));
                    if(str.length <= 7) {
                        genres = Arrays.stream(str[6].split("\\|")).map(Text::new).collect(Collectors.toList());
                    }else {
                        genres = Arrays.stream(str[str.length-1].split("\\|")).map(Text::new).collect(Collectors.toList());
                    }
                }
            } catch (ArrayIndexOutOfBoundsException exception) {
                System.out.println("Línea errónea: longitud menor a 7");
            }
            for (Text genre: genres) {
                context.write(genre, rate);
            }
        }
    }
}
