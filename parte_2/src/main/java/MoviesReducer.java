import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

public class MoviesReducer {

    public static class Reduce extends Reducer<Text, FloatWritable,Text, FloatWritable> {

        @Override
        protected void reduce(Text key, Iterable<FloatWritable> values, Context context)
                throws IOException, InterruptedException {

            float meanRate = 0;
            int i = 0;

            for (FloatWritable val : values) {
                meanRate += val.get();
                i += 1;
            }

            context.write(key, new FloatWritable(meanRate/i));
        }
    }
}

